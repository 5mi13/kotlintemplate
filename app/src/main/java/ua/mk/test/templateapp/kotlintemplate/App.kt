package ua.mk.test.templateapp.kotlintemplate

import android.app.Application
import com.facebook.stetho.Stetho
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import ua.mk.test.templateapp.kotlintemplate.configuration.configurationModule
import ua.mk.test.templateapp.kotlintemplate.data.api.apiModule
import java.io.File

class App : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        bind<File>() with singleton { cacheDir }
        import(configurationModule)
        import(apiModule)
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}