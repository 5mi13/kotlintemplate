package ua.mk.test.templateapp.kotlintemplate.configuration

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


@GlideModule
class AppGlideModule : AppGlideModule()