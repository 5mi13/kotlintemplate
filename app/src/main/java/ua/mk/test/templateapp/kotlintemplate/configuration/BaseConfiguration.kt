package ua.mk.test.templateapp.kotlintemplate.configuration

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

class BaseConfiguration : Configuration {
    private val apiUrl = "https://api.flickr.com/services/rest/"

    override fun getBaseUrl(): String = apiUrl
}

val configurationModule = Kodein.Module {
    bind<Configuration>() with singleton { BaseConfiguration() }
}