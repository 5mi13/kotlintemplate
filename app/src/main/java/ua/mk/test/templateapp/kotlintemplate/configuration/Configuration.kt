package ua.mk.test.templateapp.kotlintemplate.configuration

interface Configuration {

    fun getBaseUrl(): String
}