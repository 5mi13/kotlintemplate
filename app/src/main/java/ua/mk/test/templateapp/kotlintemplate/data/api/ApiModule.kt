package ua.mk.test.templateapp.kotlintemplate.data.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import ua.mk.test.templateapp.kotlintemplate.configuration.Configuration
import java.io.File

class ApiModule {
    fun provideOkHttpClient(cacheDir: File): OkHttpClient {
        val cacheSize: Long = 10 * 1024 * 1024
        val cache = Cache(cacheDir, cacheSize)
        return OkHttpClient.Builder()
                .addInterceptor(ApiInterceptor())
                .addNetworkInterceptor(StethoInterceptor())
                .cache(cache)
                .build()
    }

    fun provideRetrofit(configuration: Configuration, okHttpClient: OkHttpClient) =
            Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create())
                    .baseUrl(configuration.getBaseUrl())
                    .client(okHttpClient)
                    .build()

}

val apiModule = Kodein.Module {
    bind<OkHttpClient>() with singleton { ApiModule().provideOkHttpClient(instance()) }
    bind<Retrofit>() with singleton { ApiModule().provideRetrofit(instance(), instance()) }
}