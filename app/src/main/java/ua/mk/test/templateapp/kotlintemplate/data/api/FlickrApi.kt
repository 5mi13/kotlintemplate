package ua.mk.test.templateapp.kotlintemplate.data.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ua.mk.test.templateapp.kotlintemplate.models.RecentData

interface FlickrApi {
    companion object {
        const val METHOD_GET_RECENT = "flickr.photos.getRecent"
        const val METHOD_SEARCH = "flickr.photos.search"
    }

    @GET("./")
    fun getPhotos(@Query("method") method: String,
                  @Query("per_page") pageLimit: Int,
                  @Query("page") pageOffset: Int?): Observable<RecentData>

    @GET("./")
    fun search(@Query("method") method: String,
               @Query("per_page") pageLimit: Int,
               @Query("page") pageOffset: Int?,
               @Query("text") searchText: String
    ): Observable<RecentData>

}