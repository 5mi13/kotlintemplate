package ua.mk.test.templateapp.kotlintemplate.interactors

import android.content.Context
import android.text.TextUtils
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import retrofit2.Retrofit
import ua.mk.test.templateapp.kotlintemplate.configuration.GlideApp
import ua.mk.test.templateapp.kotlintemplate.data.api.FlickrApi
import ua.mk.test.templateapp.kotlintemplate.data.api.FlickrApi.Companion.METHOD_GET_RECENT
import ua.mk.test.templateapp.kotlintemplate.data.api.FlickrApi.Companion.METHOD_SEARCH
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.models.RecentData
import java.io.File


class MainInteractor(val context: Context, retrofit: Retrofit) {

    companion object {
        const val PAGE_SIZE = 24
        const val FIRST_PAGE = 1
    }

    val flickrApi = retrofit.create(FlickrApi::class.java);

    fun getPhotos(pageOffset: Int?): Observable<RecentData> =
            flickrApi.getPhotos(METHOD_GET_RECENT, PAGE_SIZE, pageOffset)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext({ photos -> preloadPhotos(photos) })
                    .map { recentData -> removeItemsWithoutPhoto(recentData) }


    fun search(searchText: String, pageOffset: Int?): Observable<RecentData> =
            flickrApi.search(METHOD_SEARCH, PAGE_SIZE, pageOffset, searchText)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext({ photos -> preloadPhotos(photos) })
                    .map { recentData -> removeItemsWithoutPhoto(recentData) }


    private fun preloadPhotos(recentData: RecentData) {
        for (photoData: PhotoData in recentData.photos.photo) {
            preloadPhotos(photoData.urlM)
        }
    }

    private fun removeItemsWithoutPhoto(recentData: RecentData): RecentData {
        val filteredPhotos = ArrayList<PhotoData>()
        val photos = recentData.photos.photo
        for (photo: PhotoData in photos) {
            if (!TextUtils.isEmpty(photo.urlL) && !TextUtils.isEmpty(photo.urlM)) {
                filteredPhotos.add(photo)
            }
        }
        recentData.photos.photo = filteredPhotos

        return recentData
    }

    private fun preloadPhotos(photo: String?) {
        GlideApp.with(context)
                .load(photo)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .preload()
    }


    fun saveSearchText(searchText: String) {
        val mapper = jacksonObjectMapper()
        val suggestions = readOrCreateSuggestionData()
        if (!suggestions.contains(searchText)) {
            suggestions.add(searchText)
        }
        mapper.writeValue(getFile(), suggestions)
    }

    private fun getFile() = File(context.cacheDir, "suggestions.json")

    fun readOrCreateSuggestionData(): ArrayList<String> {
        val mapper = jacksonObjectMapper()
        val file = getFile();
        if (file.exists()) {
            return mapper.readValue(getFile())
        } else {
            return ArrayList()
        }
    }
}