package ua.mk.test.templateapp.kotlintemplate.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty


@JsonIgnoreProperties(ignoreUnknown = true)
data class PhotoData(
        @JsonProperty("id") val id: String,
        @JsonProperty("title") val title: String,
        @JsonProperty("url_l") val urlL: String?,
        @JsonProperty("url_m") val urlM: String?
): BaseData()