package ua.mk.test.templateapp.kotlintemplate.models

import com.fasterxml.jackson.annotation.JsonProperty


data class PhotosData(@JsonProperty("page") val page: Int,
                      @JsonProperty("pages") val pages: Int,
                      @JsonProperty("perpage") val perpage: Int,
                      @JsonProperty("total") val total: Int,
                      @JsonProperty("photo") var photo: List<PhotoData> = ArrayList()): BaseData()