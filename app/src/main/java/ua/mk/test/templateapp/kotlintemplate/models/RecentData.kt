package ua.mk.test.templateapp.kotlintemplate.models

import com.fasterxml.jackson.annotation.JsonProperty


data class RecentData(
        @JsonProperty("photos") val photos: PhotosData,
        @JsonProperty("stat") val stat: String) : BaseData()