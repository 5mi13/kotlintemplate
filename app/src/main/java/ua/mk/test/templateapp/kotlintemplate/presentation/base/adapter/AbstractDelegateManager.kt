package ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup


abstract interface AbstractDelegateManager<T> {

    abstract fun getItemViewType(items: T, position: Int): Int

    abstract fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder

    abstract fun onBindViewHolder(items: T, position: Int, viewHolder: RecyclerView.ViewHolder)

}