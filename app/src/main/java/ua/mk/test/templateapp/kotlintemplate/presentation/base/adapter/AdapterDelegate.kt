package ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ua.mk.test.templateapp.kotlintemplate.models.BaseData


interface AdapterDelegate<T : BaseData> {

    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(items: T, position: Int, holder: RecyclerView.ViewHolder)
}