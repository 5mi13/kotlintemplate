package ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.View


abstract class BaseViewHolder<T>(rootView: View) : RecyclerView.ViewHolder(rootView) {

    abstract fun mapData(data: T)
}