package ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ua.mk.test.templateapp.kotlintemplate.R
import ua.mk.test.templateapp.kotlintemplate.models.BaseData


class EmptyAdapterDelegate : AdapterDelegate<BaseData> {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_empty, parent, false)
        return EmptyItemAdapterHolder(itemView)
    }

    override fun onBindViewHolder(items: BaseData, position: Int, holder: RecyclerView.ViewHolder) {}
}