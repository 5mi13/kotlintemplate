package ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.View


class EmptyItemAdapterHolder(itemView: View) : RecyclerView.ViewHolder(itemView)