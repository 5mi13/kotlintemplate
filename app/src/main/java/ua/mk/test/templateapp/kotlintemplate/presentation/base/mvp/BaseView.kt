package ua.mk.test.templateapp.kotlintemplate.presentation.base.mvp

import android.content.Context
import com.trello.rxlifecycle2.LifecycleTransformer


interface BaseView {
    fun getContext(): Context?
    fun <T> bindToLifecycle(): LifecycleTransformer<T>
}