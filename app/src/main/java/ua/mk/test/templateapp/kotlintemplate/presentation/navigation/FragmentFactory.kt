package ua.mk.test.templateapp.kotlintemplate.presentation.navigation

import android.support.v4.app.Fragment
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Screens.SCREEN_MAIN
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Screens.SCREEN_PHOTO
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.MainFragment
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoFragment
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo


object FragmentFactory {

    fun getFragmentByKeyAndPutData(screenKey: String, fragmentInfo: Any?): Fragment {
        when (screenKey) {
            SCREEN_MAIN -> return MainFragment.newInstance()
            SCREEN_PHOTO -> return PhotoFragment.newInstance(fragmentInfo as PhotoInfo)
            else -> return Fragment()
        }
    }

    fun getFragmentTagByKey(screenKey: String): String{
        when (screenKey) {
            SCREEN_MAIN -> return MainFragment::class.java.name
            SCREEN_PHOTO -> return PhotoFragment::class.java.name
            else -> return ""
        }
    }
}