package ua.mk.test.templateapp.kotlintemplate.presentation.navigation

import android.support.transition.TransitionInflater
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import ua.mk.test.templateapp.kotlintemplate.R
import ua.mk.test.templateapp.kotlintemplate.utils.UiAvailabilityUtil

class MainNavigation(val activity: AppCompatActivity, val containerId: Int) : Navigation {

    override fun openFragment(screenKey: String, fragmentInfo: Any?) {
        if (isContainerExist()) {
            val tag: String = FragmentFactory.getFragmentTagByKey(screenKey)
            val existingFragment: Fragment? = activity.supportFragmentManager.findFragmentByTag(tag)
            if (existingFragment == null) {
                if (UiAvailabilityUtil.isUiAvailable(activity)) {
                    val fragment: Fragment = FragmentFactory.getFragmentByKeyAndPutData(screenKey, fragmentInfo)
                    openFragment(tag, fragment)
                }
            }
        }
    }

    override fun openFragmentWithTransition(exitFragment: Fragment, view: View, fragmentInfo: Any?, screenKey: String) {
        if (isContainerExist()) {
            val tag: String = FragmentFactory.getFragmentTagByKey(screenKey)
            val existingFragment: Fragment? = activity.supportFragmentManager.findFragmentByTag(tag)
            if (existingFragment == null) {
                if (UiAvailabilityUtil.isUiAvailable(activity)) {
                    val fragment: Fragment = FragmentFactory.getFragmentByKeyAndPutData(screenKey, fragmentInfo)
                    val elementAnimation = TransitionInflater.from(activity).inflateTransition(R.transition.change_image_transform)
                    val exitEnterTransition = TransitionInflater.from(activity).inflateTransition(android.R.transition.fade)
                    exitFragment.sharedElementEnterTransition = elementAnimation
                    exitFragment.sharedElementReturnTransition = elementAnimation
                    exitFragment.exitTransition = exitEnterTransition

                    fragment.sharedElementEnterTransition = elementAnimation
                    fragment.sharedElementReturnTransition = elementAnimation
                    fragment.enterTransition = exitEnterTransition
                    openFragmentWithTransition(tag, fragment, view)
                }
            }
        }

    }

    private fun openFragment(tag: String, fragment: Fragment) {
        activity.supportFragmentManager.beginTransaction()
                .addToBackStack(tag)
                .add(containerId, fragment, tag)
                .commit()
    }

    private fun openFragmentWithTransition(tag: String, fragment: Fragment, sharedElement: View) {
        activity.supportFragmentManager.beginTransaction()
                .addToBackStack(tag)
                .replace(containerId, fragment, tag)
                .addSharedElement(sharedElement, ViewCompat.getTransitionName(sharedElement))
                .commit()
    }

    fun isContainerExist(): Boolean = activity.findViewById<View>(containerId) != null
}