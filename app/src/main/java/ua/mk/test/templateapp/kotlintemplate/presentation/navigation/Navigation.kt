package ua.mk.test.templateapp.kotlintemplate.presentation.navigation

import android.support.v4.app.Fragment
import android.view.View


interface Navigation {

    fun openFragment(screenKey: String, fragmentInfo: Any?)

    fun openFragmentWithTransition(exitFragment: Fragment, view: View, fragmentInfo: Any?, screenKey: String)
}