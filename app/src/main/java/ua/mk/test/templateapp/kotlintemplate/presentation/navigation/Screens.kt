package ua.mk.test.templateapp.kotlintemplate.presentation.navigation

object Screens {
    const val SCREEN_MAIN = "SCREEN_MAIN"
    const val SCREEN_PHOTO = "SCREEN_PHOTO"
}