package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main

import android.widget.ImageView


interface ImageClickAction<T> {
    fun openImage(imageView: ImageView, item: T)
}