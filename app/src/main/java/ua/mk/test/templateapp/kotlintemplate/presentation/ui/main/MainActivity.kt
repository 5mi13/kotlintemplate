package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import org.kodein.di.Copy
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinContext
import org.kodein.di.android.activityRetainedScope
import org.kodein.di.android.closestKodein
import org.kodein.di.android.retainedKodein
import org.kodein.di.generic.*
import ua.mk.test.templateapp.kotlintemplate.R
import ua.mk.test.templateapp.kotlintemplate.interactors.MainInteractor
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.MainNavigation
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Navigation
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Screens
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp.MainActivityContract
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp.MainActivityPresenter

class MainActivity : RxAppCompatActivity(), KodeinAware, MainActivityContract.View {

    override val kodeinContext: KodeinContext<*> get() = kcontext(this)
    private val appKodein by closestKodein()
    override lateinit var kodein: Kodein
    private val kodeinActivity: Kodein by retainedKodein {
        extend(appKodein, copy = Copy.All)
        bind<AppCompatActivity>() with scoped(activityRetainedScope).singleton { this@MainActivity }
        bind<Navigation>() with scoped(activityRetainedScope).singleton {
            return@singleton MainNavigation(instance(), R.id.fragmentContainer)
        }
        bind<MainActivityContract.View>() with singleton {this@MainActivity}
        bind<MainInteractor>() with singleton { MainInteractor(instance(), instance()) }
        bind<MainActivityContract.Presenter>() with scoped(activityRetainedScope).singleton { MainActivityPresenter(instance(), instance()) }
    }

    val presenter : MainActivityContract.Presenter by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        kodein = kodeinActivity.kodein
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        presenter.navigateTo(Screens.SCREEN_MAIN)
    }

    override fun onBackPressed() {
        val isLastFragment = supportFragmentManager.backStackEntryCount == 1
        if (isLastFragment) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    override fun getContext() = this@MainActivity
}