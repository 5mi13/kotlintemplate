package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.jakewharton.rxbinding2.support.v7.widget.scrollEvents
import com.trello.rxlifecycle2.components.support.RxFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_main.*
import org.kodein.di.Copy
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinContext
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.kcontext
import org.kodein.di.generic.provider
import ua.mk.test.templateapp.kotlintemplate.R
import ua.mk.test.templateapp.kotlintemplate.R.id.inputSearch
import ua.mk.test.templateapp.kotlintemplate.R.id.recyclerGallery
import ua.mk.test.templateapp.kotlintemplate.interactors.MainInteractor.Companion.PAGE_SIZE
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.adapter.PhotoAdapter
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp.MainContract
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp.MainPresenter
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo
import java.util.concurrent.TimeUnit


class MainFragment : RxFragment(), MainContract.View, KodeinAware, ImageClickAction<PhotoInfo>, TextView.OnEditorActionListener {

    override val kodeinContext: KodeinContext<*> get() = kcontext(activity)

    private val kodeinParent by closestKodein()
    override val kodein = Kodein.lazy {
        extend(kodeinParent, copy = Copy.All)
        bind<MainContract.View>() with provider { this@MainFragment }
        bind<MainContract.Presenter>() with provider { MainPresenter(instance(), instance(), instance()) }
    }

    val presenter: MainContract.Presenter by instance()
    internal var photoAdapter = PhotoAdapter(::openImage)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_main, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearch()
        setupGallery()
        if (photoAdapter.itemCount == 0) {
            presenter.getPhotos()
        }
    }

    private fun setupSearch() {
        inputSearch.setOnEditorActionListener(this@MainFragment)
        updateSuggestions()
    }

    override fun updateSuggestions() =
            inputSearch.setAdapter(ArrayAdapter(context,
                    android.R.layout.simple_dropdown_item_1line, presenter.getSuggestions()))


    override fun onEditorAction(view: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            presenter.clearData()
            photoAdapter.clear()
            presenter.search(view?.text.toString());
            val inputManager = view
                    ?.getContext()
                    ?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val binder = view.getWindowToken()
            inputManager.hideSoftInputFromWindow(binder,
                    InputMethodManager.HIDE_NOT_ALWAYS)
            return true;
        }
        return false;
    }

    private fun setupGallery() {
        val gridLayoutManager = GridLayoutManager(context, TWO_COLUMNS)
        recyclerGallery.layoutManager = gridLayoutManager
        recyclerGallery.adapter = photoAdapter
        recyclerGallery.scrollEvents()
                .debounce(250, TimeUnit.MILLISECONDS)
                .filter({ scroll -> scroll.dy() > 0 })
                .map({ result -> gridLayoutManager.findLastVisibleItemPosition() })
                .distinctUntilChanged()
                .filter({ lastVisibleItemPosition -> lastVisibleItemPosition >= (photoAdapter.itemCount - (PAGE_SIZE / 2)) })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindToLifecycle())
                .subscribe({ lastVisibleItemPosition ->
                    if (inputSearch.text.isEmpty())
                        presenter.getPhotos()
                    else
                        presenter.search(inputSearch.text.toString())
                })
    }

    override fun addData(photos: List<PhotoData>) = photoAdapter.addData(photos)

    override fun openImage(imageView: ImageView, item: PhotoInfo) =
            presenter.openImage(this@MainFragment, imageView, item)

    companion object {
        const val TWO_COLUMNS = 2;
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }
}