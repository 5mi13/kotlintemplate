package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo
import java.util.*
import kotlin.reflect.KFunction2


class PhotoAdapter(imageClickAction: KFunction2<ImageView, PhotoInfo, Unit>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val delegateManager = PhotoDelegateManager(imageClickAction)

    private var photos: ArrayList<PhotoData> = ArrayList()

    override fun getItemViewType(position: Int): Int {
        return delegateManager.getItemViewType(photos.get(position), position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateManager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateManager.onBindViewHolder(photos.get(position), position, holder)
    }

    override fun getItemCount(): Int = photos.size


    fun addData(data: List<PhotoData>) {
        if (photos !== data) {
            val currentSize = photos.size + 1
            for (photo: PhotoData in data) {
                if (!photos.contains(photo)) {
                    photos.add(photo)
                }
            }
            notifyItemRangeInserted(currentSize, photos.size - currentSize)
        }
    }

    fun clear() = photos.clear()
}