package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import ua.mk.test.templateapp.kotlintemplate.R
import ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter.AdapterDelegate
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo
import kotlin.reflect.KFunction2


class PhotoAdapterDelegate(val imageClickAction: KFunction2<ImageView, PhotoInfo, Unit>) : AdapterDelegate<PhotoData> {
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_photo, parent, false)
        return PhotoViewHolder(itemView, imageClickAction)
    }

    override fun onBindViewHolder(items: PhotoData, position: Int, holder: RecyclerView.ViewHolder) {
        val photoViewHolder = holder as PhotoViewHolder
        photoViewHolder.mapData(items)
    }
}