package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter.AbstractDelegateManager
import ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter.AdapterDelegate
import ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter.EmptyAdapterDelegate
import ua.mk.test.templateapp.kotlintemplate.models.BaseData
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo
import kotlin.reflect.KFunction2


class PhotoDelegateManager(imageClickAction: KFunction2<ImageView, PhotoInfo, Unit>) : AbstractDelegateManager<PhotoData> {

    private val ITEM_PHOTO = 10
    private val photoAdapterDelegate = PhotoAdapterDelegate(imageClickAction) as AdapterDelegate<BaseData>

    override fun getItemViewType(items: PhotoData, position: Int): Int = ITEM_PHOTO

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return getAdapterDelegate(viewType).onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(items: PhotoData, position: Int, viewHolder: RecyclerView.ViewHolder) {
        val itemViewType = viewHolder.itemViewType
        val delegate = getAdapterDelegate(itemViewType)
        delegate.onBindViewHolder(items, position, viewHolder)
    }

    private fun getAdapterDelegate(itemViewType: Int): AdapterDelegate<BaseData> {
        when (itemViewType) {
            ITEM_PHOTO -> return photoAdapterDelegate
            else -> return EmptyAdapterDelegate()
        }
    }
}