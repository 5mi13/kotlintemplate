package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.adapter

import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.item_photo.view.*
import ua.mk.test.templateapp.kotlintemplate.configuration.GlideApp
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.presentation.base.adapter.BaseViewHolder
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo
import kotlin.reflect.KFunction2


class PhotoViewHolder(itemView: View, val imageClickAction: KFunction2<ImageView, PhotoInfo, Unit>) : BaseViewHolder<PhotoData>(itemView) {

    override fun mapData(data: PhotoData) {
        onLoadingStart()
        GlideApp.with(itemView.context)
                .load(data.urlL)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .listener(onImageLoadListener(data))
                .into(itemView.imagePhoto)

        val photoInfo = PhotoInfo(data.id, data.urlM)
        itemView.imagePhoto.transitionName = data.id
        itemView.imagePhoto.setOnClickListener({ imageView -> imageClickAction(imageView as ImageView, photoInfo) })
    }

    private fun onImageLoadListener(data: PhotoData): RequestListener<Drawable> = object : RequestListener<Drawable> {
            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                onLoadingEnd(data.title)
                return false
            }

            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                onLoadingEnd(data.title)
                itemView.imagePhoto.setImageDrawable(resource)
                return true
            }
        }

    private fun onLoadingStart() {
        itemView.titlePhoto.visibility = GONE
        itemView.progress.visibility = VISIBLE
    }

    private fun onLoadingEnd(title : String) {
        itemView.progress.visibility = GONE
        itemView.titlePhoto.visibility = if (TextUtils.isEmpty(title)) GONE else VISIBLE
        itemView.titlePhoto.text = title
    }
}