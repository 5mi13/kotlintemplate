package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp

import ua.mk.test.templateapp.kotlintemplate.presentation.base.mvp.BasePresenter
import ua.mk.test.templateapp.kotlintemplate.presentation.base.mvp.BaseView

interface MainActivityContract {
    interface View: BaseView

    interface Presenter: BasePresenter {
        fun navigateTo(screen: String)
    }
}