package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp

import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Navigation
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Screens

class MainActivityPresenter(val view: MainActivityContract.View, val navigation: Navigation) : MainActivityContract.Presenter {

    override fun navigateTo(screen: String) {
        navigation.openFragment(Screens.SCREEN_MAIN, null)
    }
}