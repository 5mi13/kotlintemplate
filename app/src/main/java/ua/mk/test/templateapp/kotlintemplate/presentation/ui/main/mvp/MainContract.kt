package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp

import android.support.v4.app.Fragment
import android.widget.ImageView
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.presentation.base.mvp.BasePresenter
import ua.mk.test.templateapp.kotlintemplate.presentation.base.mvp.BaseView
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo


interface MainContract {
    interface View: BaseView {
        fun addData(photos: List<PhotoData>)
        fun updateSuggestions()
    }

    interface Presenter: BasePresenter {
        fun getPhotos()
        fun search(searchText : String)
        fun clearData()
        fun getSuggestions(): Array<String>
        fun openImage(exitFragment: Fragment, imageView: ImageView, photoInfo: PhotoInfo)
    }
}