package ua.mk.test.templateapp.kotlintemplate.presentation.ui.main.mvp

import android.support.v4.app.Fragment
import android.util.Log
import android.widget.ImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ua.mk.test.templateapp.kotlintemplate.interactors.MainInteractor
import ua.mk.test.templateapp.kotlintemplate.interactors.MainInteractor.Companion.FIRST_PAGE
import ua.mk.test.templateapp.kotlintemplate.models.PhotoData
import ua.mk.test.templateapp.kotlintemplate.models.RecentData
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Navigation
import ua.mk.test.templateapp.kotlintemplate.presentation.navigation.Screens.SCREEN_PHOTO
import ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo.PhotoInfo


class MainPresenter(val view: MainContract.View, val mainInteractor: MainInteractor, val mainNavigation: Navigation) : MainContract.Presenter {

    var TAG = MainPresenter::class.java.name
    var recentData: RecentData? = null
    var nextPage = if (recentData != null) recentData?.photos?.page?.inc() else FIRST_PAGE

    override fun getPhotos() {
        mainInteractor.getPhotos(nextPage)
                .observeOn(Schedulers.io())
                .doOnNext({ recentData = it })
                .map({ it.photos.photo })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(view.bindToLifecycle())
                .subscribe({ this.updateData(it) }, { this.handleError(it) })
    }

    override fun search(searchText: String) {
        saveAndUpdateSuggestion(searchText)
        mainInteractor.search(searchText, nextPage)
                .observeOn(Schedulers.io())
                .doOnNext({ recentData = it })
                .map({ it.photos.photo })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(view.bindToLifecycle())
                .subscribe({ this.updateData(it) }, { this.handleError(it) })
    }

    private fun saveAndUpdateSuggestion(searchText: String) {
        mainInteractor.saveSearchText(searchText)
        view.updateSuggestions()
    }

    private fun updateData(photos: List<PhotoData>) =
            view.addData(photos)

    private fun handleError(throwable: Throwable?) =
            Log.e(TAG, throwable?.message)

    override fun getSuggestions(): Array<String> =
            mainInteractor.readOrCreateSuggestionData().toTypedArray()

    override fun openImage(exitFragment: Fragment, imageView: ImageView, photoInfo: PhotoInfo) =
            mainNavigation.openFragmentWithTransition(exitFragment, imageView, photoInfo, SCREEN_PHOTO)

    override fun clearData() {
        recentData = null
    }

}