package ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.trello.rxlifecycle2.components.support.RxFragment
import kotlinx.android.synthetic.main.fragment_image.*
import ua.mk.test.templateapp.kotlintemplate.R
import ua.mk.test.templateapp.kotlintemplate.R.id.imagePhoto
import ua.mk.test.templateapp.kotlintemplate.configuration.GlideApp


class PhotoFragment : RxFragment() {

    companion object {
        const val ARGUMENT_FRAGMENT_DATA = "ARGUMENT_FRAGMENT_DATA"
        fun newInstance(photoInfo: PhotoInfo): PhotoFragment {
            val fragment = PhotoFragment()
            val bundle = Bundle()
            bundle.putParcelable(ARGUMENT_FRAGMENT_DATA, photoInfo)
            fragment.setArguments(bundle)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GlideApp.with(context!!)
                .load(photoUrl())
                .onlyRetrieveFromCache(true)
                .into(imagePhoto)
        imagePhoto.setTransitionName(transitionName())
    }

    fun fragmentDataArgument(): PhotoInfo? = arguments?.getParcelable(ARGUMENT_FRAGMENT_DATA)

    fun transitionName(): String? = fragmentDataArgument()?.transitionName

    fun photoUrl(): String? = fragmentDataArgument()?.photoUrl

}