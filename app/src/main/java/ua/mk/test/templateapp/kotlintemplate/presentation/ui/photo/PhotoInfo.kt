package ua.mk.test.templateapp.kotlintemplate.presentation.ui.photo

import android.os.Parcel
import android.os.Parcelable


data class PhotoInfo(val transitionName: String, val photoUrl: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(transitionName)
        parcel.writeString(photoUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PhotoInfo> {
        override fun createFromParcel(parcel: Parcel): PhotoInfo {
            return PhotoInfo(parcel)
        }

        override fun newArray(size: Int): Array<PhotoInfo?> {
            return arrayOfNulls(size)
        }
    }
}