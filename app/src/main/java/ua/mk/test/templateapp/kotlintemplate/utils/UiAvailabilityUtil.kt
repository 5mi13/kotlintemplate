package ua.mk.test.templateapp.kotlintemplate.utils

import android.app.Activity
import android.content.Context
import android.support.v4.app.Fragment

object UiAvailabilityUtil {

    fun isUiAvailable(context: Context?): Boolean =
            context != null && context is Activity && isUiAvailable(context as Activity?)


    fun isUiAvailable(activity: Activity?): Boolean =
            activity != null && !activity.isFinishing && !activity.isDestroyed


    fun isUiAvailable(fragment: Fragment?): Boolean =
            fragment != null && fragment.isAdded && isUiAvailable(fragment.activity)

}